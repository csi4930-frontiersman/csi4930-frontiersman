# CSI4930 Frontiersman

## README Requirements for class

Description of Project: 

A clone of Settlers of CATAN (base game, 1-4 players local multiplayer)
The game rules are included in the Help Doc.pdf

Package Name on PyPI: Frontiersman

Executable Line to Run Server: FMServer

Executable Line to Run Clients: FMClient

Gitlab Link: [https://gitlab.com/csi4930-frontiersman/csi4930-frontiersman](https://gitlab.com/csi4930-frontiersman/csi4930-frontiersman)

## Notes
FMServer must be running in order to player. Multiple clients should connect. The server can also host multiple games using different invite codes.